# Feasability report SSIComms

## 1. INTRODUCTION
One of the things people enjoy the most about the internet, is that it enables them to talk to others remotely almost without limit. Unfortunately, remotely often means that parties are not sure who they are communicating with.

This project solves this problem of *identified communications* by involving SSI and by starting from the SIP communications protocol, the protocol that underlies most VOIP telephony. The SIP protocol (and SSIComms) supports so called rich communications, meaning voice and video calling, chat sessions, desktop sharing, group sessions, etc.

Another solution to achieving identified communications would have been to start from SSI by establishing a DIDComm connection first, but this has a number of drawbacks.

### 1.1. How SSIComms works
SSIComms enables the exchange of verifiable credentials by introducing a DIDCOMM session parallel to the communications session in 4 major steps:
- First, Alice and Bob start a SIP session, for instance by Alice calling Bob using the SIP protocol.
- Then, after Bob answers, he and Alice establish a DIDComm connection by Bob sending Alice an out-of band-message. As a means of transport, Bob chooses the active SIP session between him and Alice, and sends Alice the message as an encrypted SIP message.
- With the DIDComm connection in place, they can now use DIDComm to exchange credentials.
- The credentials proven to be satisfactory, Bob and Alice continue their SIP session and exchange voice, video or text messages.

Because of its “SIP first approach”, SSIComms is able to facilitate identified communications sessions with both participants behind a firewall. Under these circumstances, DIDComm by itself would not enable Alice to contact Bob.
What is more, the clients from the SSIComms project have the enormous advantage of backward compatibility with 100s of millions of devices (like office desk phones) and software clients, and allows them to set up various unidentified communications sessions with most of these out of the box.

## 2. TECHNICAL ACCOMPLISHMENTS
Our technical accomplishments can be summarized as follows:
### 2.1. Adding SSI awareness to SYLK
- During the initial INVITE and 200OK response the SYLK/SSIComms app is able to signal the support for SSI by using a special header called "SSI-roles". 

### 2.2. Integration of SYLK and the Animo SSI module into the SYLK/SSIComms app
The integration of SYLK and the Animo SSI module together with a business module into the SYLK/SSIComms app has resulted in the following functionalities: 

- If both parties support SSI, then a SIP Message containing a DIDComm out-of-band messag will be sent after the call is answered. This establishes a DIDComm connection at the beginning of the call.
- The SYLK/SSIComms app can distinguish between regular SIP text messages and DIDComm out of band messages.
- The SYLK/SSIComms app can pass on SIP text messages that are DIDComm out of band messages to its SSI module, to be processed.
- The SSI module can pass on DIDComm messages to the business module of the SYLK/SSIComms app, to be displayed.
- The SSI module can establish a DIDComm connection between SSSI-able participants in a SIP session
- The business module can display credentials received from a verification.

### 2.3. Addition of a QR-code reader to the SYLK/SSIComms app
- The SYLK/SSIComms app is also able to scan QR-codes, to establish a DIDComm connection with agents that are not able to engage in a SIP session. This means that the SSIComms app is also able to receive credentials from issuers in the more common way, through QR-codes.

## 3. USE CASES/KPI’S

The achieved functionalities allow for the following scenarios:
##### Use Case 1: Alice calls Bob and Bob wonders whether she is really Alice.
- Alice connects to an issuer by establishing a DIDComm connection via a QR-code, and then receiving her credential.
- Alice receives a call from Bob which starts a SIP session between the two of them.
- A DIDComm connection is established by Bob sending a out-of-band message as a SIP-text message to Alice.
- Bob invites Alice to identify herself using the established DIDComm connection.

##### Use Case 2: Alice calls Bob, Bob wonders whether Alice is Alice, and Alice wonders whether Bob is Bob
- Same steps as above with the addition of Alice identifying Bob.

##### Use Case 3: integration of Visma Mandate Service: Bob provides Alice with a mandate
- Bob connects to Alice in a SIP session.
- Bob then identifies Alice as being Alice.
- Satisfied with having established her identity, Bob sends an out-of-band DIDComm url to Alice as a SIP text message.
- Alice passes the url to her SSI module by clicking on it and establishes a DIDComm connection with the Visma Mandate Service. 
- Alice then receives her mandate from the Visma Mandate Service.


##### Use Case 4: Onboarding: providing Alice with real world credentials like IDIN, KVK and a credit score.
- Alice connects to an issuer able to issue real world credentials by establishing a DIDComm connection using her SSIComms QR-reader to read a QR-code 
- Alice then receives her credentials.

We defined the functioning of these use cases as being our KPI’s. Use case 1, 2, 3 and 4 are all working, which means that we were successful in achieving our goals.

## 4. INTEROPERABILITY 
- Interoperability was achieved with the AnimoMobile SDK by integrating it and using it as its SSI module, which is described above.
- Interoperabilty was also achieved with Visma’s Mandate Service, see above.
- SSIComms can equally accept credentials from issuers, by using its QR-Code reader. 

## 5. PUBLICATIONS
- A publication proposal has been accepted by the [RWOT conference](https://github.com/WebOfTrustInfo/rwot11-the-hague/) on the 26-30th of September in The Hague entitled *“SSI and internet communications or internet communications and SSI: comparing different solutions to the same problem”*. This paper hopes to compare the different approaches to *Identified communications*, with SSIComms having the advantage of backward compatibility.
- A proposal for a speech/demonstration on Identified Communications has been submitted to the [Malaga Conference](https://live.empodera.org/live/en), again on the same topic. 
- A still confidential proposal for a 6 month demonstration project involving mobility and parts of the SSIComms solution is currently in the phase of adding enough contributors to be able to proceed. 
- Standards contribution: during the initial INVITE and 200OK response the SYLK/SSIComms app is able to signal the support for SSI by using a special header called "SSI-roles". This way of signalling being "SSI-ready" has the potential of being used generally by SSI-able SIP applications. 

## 6. CODE
The source code for the SSIComms app can be found at https://github.com/AGProjects/sylk-mobile/
